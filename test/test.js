var XMLExporter = require('../index.js');
var fs = require('fs');

var should = require('chai').should();
var expect = require('chai').expect;

describe('XML exporter test', function(){
	it('test export functionality',function(){
		var resultXML = fs.readFileSync(__dirname + '/testResult.xml', {encoding: 'utf8'});
		resultXML = resultXML.replace(/(\r\n|\n|\r)/gm,"");
		resultXML = resultXML.replace(/\t/g, "");
		
		var testData = fs.readFileSync(__dirname + '/testdata.json', {encoding: 'utf8'});
		testData = JSON.parse(testData);
		
		var exportedXML = XMLExporter.getExperimentRepresentation(testData);
		exportedXML = exportedXML.replace(/(\r\n|\n|\r)/gm,"");
		exportedXML = exportedXML.replace(/\t/g, "");
		
		exportedXML.should.equal(resultXML);
	});
	
});