# Usage
`getExperimentRepresentation(experiment: Object)` is used to convert an experiment to an xml representation.
	experiment = {
		connections: [Connection],
		models: [Model],
		sequences: [Sequence]
	}

	Connection = {
		from: String,
		to: String
	}

	Model = {
		cid: String,
		name: String,
		Parameters: [{
			name: String,
			type: String,
			value: String,
			unit: String (optional)
		}]

	Sequence = {
		Sequence: String
	}

# Tests
After Downloading
    npm install
    npm install -g istanbul

run the command
    npm test
	
in terminal.