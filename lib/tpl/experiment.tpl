<experiment>
	<steps> <% _.forEach(models, function(model){ %> 
		<%= model %> <% }); %> 
	</steps>
	<connections><% _.forEach(connections, function(connection){ %>
		<%= connection %><% }); %>
	</connections>
	<sequences><% _.forEach(sequences, function(sequence){ %>
		<%= sequence %><% }); %>
	</sequences>
</experiment>