/* global __dirname */

var fs = require('fs');
var _ = require('lodash');

/**
 * makes a template file ready for usage.
 */
function __getTemplateFromFile(path){
	var template = fs.readFileSync(path);
	return _.template(template);
}

module.exports = {
	/**
	 * get the XML representation for the given parameter
	 */
	getParameterRepresentation: function(parameter){
		var template = __getTemplateFromFile(__dirname + '/tpl/numberParameter.tpl');
		if(parameter.type === 'enum'){
			template = __getTemplateFromFile(__dirname + '/tpl/enumParameter.tpl');
		}
		
		parameter.name =  parameter.name || '';
		parameter.value = parameter.value || '';

		return template(parameter);
	},
	
	/**
	 * Ges the XML representation for the given model
	 */
	getModelRepresentation: function(model){
		var parameters = {};
		if(!!model.parameters){
			parameters = model.parameters;
		} 
		
		var filledParameters = [];
		for(var key in parameters){
			var result = this.getParameterRepresentation(parameters[key]);
			filledParameters.push(result);
		}
		
		var modelData = {
			cid: model.cid,
			name: model.name,
			parameters: filledParameters
		};
		
		var template = __getTemplateFromFile(__dirname + '/tpl/model.tpl');
		return template(modelData);
	},
	
	/**
	 * Get the XML representation for the given connection
	 */
	getConnectionRepresentation: function(connection){
		var template = __getTemplateFromFile(__dirname + '/tpl/connection.tpl');
		return template(connection);
	},
	
	/**
	 * Get the XML representation of a sequence
	 */
	getSequenceRepresentation: function(sequence){
		var template = __getTemplateFromFile(__dirname + '/tpl/sequence.tpl');
		return template(sequence);
	},
	
	/**
	 * Get the XML representation for the given experiment
	 * experiment should be of the form:
	 * {
	 * 	connections: [connection],
	 *  models: [model],
	 *  sequences: [sequences]
	 * }
	 */
	getExperimentRepresentation: function(experiment){
		// get the representation for the models.
		var models = []
		if(!!experiment.models)
			models = experiment.models;
		
		var filledModels = [];
		
		for(var i = 0; i < models.length; i++){
			var result = this.getModelRepresentation(models[i])
			filledModels.push(result);
		}
		
		// get the representation for the connections
		var connections = []
		if(!!experiment.connections)
			connections = experiment.connections;
		
		var filledConnections = [];
		for(var j = 0; j < connections.length; j++){
			var result1 = this.getConnectionRepresentation(connections[j]);
			filledConnections.push(result1);
		}
		
		var sequences = [];
		if(!!experiment.sequences)
			sequences = experiment.sequences;
		
		var filledSequences = [];
		for(var k = 0; k < sequences.length; k++){
			var result2 = this.getSequenceRepresentation(sequences[k]);
			filledSequences.push(result2);
		}
		
		
		var experimentData = {
			models: filledModels,
			connections: filledConnections,
			sequences: filledSequences
		}
		
		var template = __getTemplateFromFile(__dirname + '/tpl/experiment.tpl');
		var xml =  template(experimentData);
		return xml;
	}
}